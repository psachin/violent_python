"""
Find & move similar Images using Python & OpenCV.

- Move the low resolution files to 'deleted' dir.
- Create a data file to save hashes. Useful to load during second run.
- Inspired from https://www.pyimagesearch.com/author/adrian/

- TODO:
  - [X] Multithreading: For faster execution over huge datasets
  - [ ] Load already saved hashes: Save time iterating over same dataset when something fails.
"""
import argparse
import concurrent.futures
import logging
import os
import shutil

import numpy as np
from imutils import paths
import cv2


format = "%(asctime)s: %(message)s"
logging.basicConfig(format=format, level=logging.INFO,
                    datefmt="%H:%M:%S")
hashes = {}


def dhash(imagePath, hashSize):
    # convert the image to grayscale and resize the grayscale image,
    # adding a single column (width) so we can compute the horizontal
    # gradient
    logging.info("Reading {}".format(imagePath))
    image = cv2.imread(imagePath)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    resized = cv2.resize(gray, (hashSize + 1, hashSize))
    # compute the (relative) horizontal gradient between adjacent
    # column pixels
    diff = resized[:, 1:] > resized[:, :-1]
    # convert the difference image to a hash and return it
    h = sum([2 ** i for (i, v) in enumerate(diff.flatten()) if v])
    p = hashes.get(h, [])
    p.append(imagePath)
    hashes[h] = p


def low_res_image(hps):
    resolutions = {}
    for p in hps:
        (h, w, c) = cv2.imread(p).shape
        logging.info("Path: {}, Height: {} Width: {}".format(p, h, w))
        resolutions[p] = h*w

    max_res = set([max(resolutions.values())])
    all = set(resolutions.values())
    all_images = list(set(resolutions.keys()))

    # Ignore high resolution images. All low resolution images goes into
    # deleted_images
    deleted_res = all - max_res
    deleted_images = []
    for p, res in resolutions.items():
        if res in deleted_res:
            deleted_images.append(p)

    if deleted_images:
        return deleted_images
    # If resolutions are same, delete all simialr images except first.
    return all_images[1:]


# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--dataset", required=True,
                help="path to input dataset")
ap.add_argument("-m", "--move", action='store_true',
                help="Move duplicate images to 'deleted' directory.")
ap.add_argument("-s", "--hashsize", type=int, default=8,
                help="Hash Size. Default is 8. Try not to exceed the avg. \
                value of Height & Width.")
ap.add_argument("-w", "--workers", type=int, default=1,
                help="Max workers. Default is 1")
args = vars(ap.parse_args())


# grab the paths to all images in our input dataset directory and
# then initialize our hashes dictionary
logging.info("Computing image hashes...")

# loop over our image paths
img_paths = list(paths.list_images(args["dataset"]))
with concurrent.futures.ThreadPoolExecutor(max_workers=args['workers']) as executor:
    # (psachin )Pass the same hashsize for every imgpath. Don't know how to
    # implement this in an elegant way.
    executor.map(dhash, img_paths, [args['hashsize'] for i in range(len(img_paths))])

# Save the hashed dataset
with open('dataset.json', 'w') as f:
    f.write(str(hashes))

# loop over the image hashes
for (h, hashedPaths) in hashes.items():
    # check to see if there is more than one image with the same hash
    if len(hashedPaths) > 1:
        # check to see if this is a dry run
        if args["move"] is False:
            # initialize a montage to store all images with the same
            # hash
            montage = None
            # loop over all image paths with the same hash
            for p in hashedPaths:
                # load the input image and resize it to a fixed width
                # and heightG
                image = cv2.imread(p)
                image = cv2.resize(image, (500, 500))
                # if our montage is None, initialize it
                if montage is None:
                    montage = image
                    # otherwise, horizontally stack the images
                else:
                    montage = np.hstack([montage, image])
                    # show the montage for the hash & dup image path
                    logging.info("hash: {} path {}".format(h, hashedPaths))
                    cv2.imshow("Montage", montage)
                    cv2.waitKey(0)
        # otherwise, we'll be removing the duplicate images
        else:
            # Create a dir to move low resolution images
            try:
                deleted_images_path = os.path.join(args["dataset"], 'deleted')
                os.mkdir(deleted_images_path)
            except FileExistsError:
                pass
            else:
                logging.info("Successfully created %s", deleted_images_path)

            # loop over all image paths with the same hash *except*
            # for the first image in the list (since we want to keep
            # one, and only one, of the duplicate images)
            for p in low_res_image(hashedPaths):
                try:
                    logging.info("Deleting {}".format(p))
                    shutil.move(p, deleted_images_path)
                except shutil.Error as e:
                    logging.info(e)
                    deleted_images_path = os.path.join(deleted_images_path, "deleted_" + os.path.basename(p))
                    shutil.move(p, deleted_images_path)
